<?php
class Department extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('department_model');
	}
		
	public function index(){

		$this->load->view('header_view');
		$data = $this->department_model->getDepts();
		$this->load->view('department_view', array('data'=>$data));
	}

	public function add(){
		$this->load->view('header_view');
		$this->load->view('add_department_view');
	}
	
	public function addDept(){
		$data=$this->department_model->addDept($_POST['dept']);
	}
	
	public function delete() {
		$this->department_model->deleteDept($_GET['id']);
	} 
}