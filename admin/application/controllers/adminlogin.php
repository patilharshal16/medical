<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adminlogin extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('adminlogin_model');
		$this->load->helper('url');
	}

		public function index(){
			$this->load->view('adminLogin_view');
		}
		
		public function verify(){
			$data=$this->adminlogin_model->authenticate($_POST['username'],$_POST['password']);
			if ($data == 0){
				header("Location:".base_url());
			}else {
				$this->load->view('header_view');
			}
		}
		
}