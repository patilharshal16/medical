<?php
class Report extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('report_model');
		$this->load->view('header_view');
	}
	public function index(){
		$data=$this->report_model->getReport();
		$this->load->view('report_view',array('data' => $data));
	}
	
	public function addView()
	{
		$this->load->model('department_model');
		$data = $this->department_model->getDepts();
		$this->load->view('addReport_view',array('department' => $data));
	}
	
	public function add(){
		$this->report_model->addReport($_POST['report'],$_POST['department']);
	}
	
	public function delete(){
		$this->report_model->deleteReport($_GET['id']);
		
	}
}