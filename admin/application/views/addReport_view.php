<div class="container">
	<form method="post" action="<?php echo base_url();?>report/add">

		<fieldset>
			<legend>Add Report</legend>
			<label>Report Name</label>
			<input type="text" id="report" name="report"
					placeholder="Enter the report Name" class="span6"><br>
			<label>Select Department</label>
			<select name="department" id="department" class="span6">
			<?php foreach ($department as $data){?>
			<option value="<?php echo $data->dept_id;?>"><?php echo $data->departmentName;?></option>
			<?php }?>
			</select><br>
			<button type="submit" class="btn">Submit</button>
		</fieldset>
	</form>
</div>
