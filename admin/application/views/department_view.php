<div class="container">

	<div class="row">
		<h1>Department</h1>
		<br> <a class="btn btn-primary"
			href="<?php echo base_url();?>department/add">Add Department</a><br><br>
			<p>See below the list of Department currently listed</p><br>
	</div>
	<table class="table table-striped table-content">
		<thead>
			<tr>
				<th>No</th>
				<th>Department Name</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
					<?php $i=1;foreach ($data as $d){ ?>
					<td><?php echo $i++; ?></td>
					<td><?php echo $d->departmentName?></td>
					<td><a href="<?php echo base_url();?>department/delete?id=<?php echo $d->dept_id;?>" class="btn btn-danger btn-delete">Delete</a></td>
				</tr>
				<?php }?>
		</tbody>
	</table>
</div>