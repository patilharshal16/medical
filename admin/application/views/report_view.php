<div class="container">

	<div class="row">
		<h1>Reports</h1>
		<br> <a class="btn btn-primary"
			href="<?php echo base_url();?>report/addView">Add Report</a><br><br>
			<p>See below the list of Reports currently listed</p><br>
	</div>
	<table class="table table-striped table-content">
		<thead>
			<tr>
				<th>No</th>
				<th>Report Type</th>
				<th>Department Name</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
					<?php $i=1;foreach ($data as $d){ ?>
					<td><?php echo $i++; ?></td>
					<td><?php  echo $d->report_name; ?></td>
					<td><?php echo $d->departmentName?></td>
					<td><a href="<?php echo base_url();?>report/delete?id=<?php echo $d->report_id;?>" class="btn btn-danger btn-delete">Delete</a></td>
				</tr>
				<?php }?>
		</tbody>
	</table>
</div>