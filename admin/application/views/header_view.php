<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet"
	href="<?php echo base_url();?>css/bootstrap.min.css" />
	<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/script.js"></script>
<title>Admin Medical</title>
<style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>

</head>

<body>

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="brand" href="#">My Medical Diary</a>
				<div class="nav-collapse collapse">
					<ul class="nav">
						<li><a href="#">Home</a></li>
						<li><a href="<?php echo base_url();?>department">Department</a></li>
					<!-- 	<li><a href="<?php echo base_url();?>report">Reports</a></li>
						<li><a href="<?php echo base_url();?>history">History</a></li> -->
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>

	<!-- <ul class="nav nav-tabs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>department">Department</a></li>
	<li><a href="<?php echo base_url();?>department">Sub Departments</a></li>
</ul> -->