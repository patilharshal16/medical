<div class="container">
	<form method="post" action="<?php echo base_url();?>department/addDept"
		class="">

		<fieldset>
			<legend>Add Department</legend>
			<label>Department Name</label>
			<input type="text" id="dept" name="dept"
					placeholder="Enter the Department Name" class="span6">
			<span class="help-block">Enter a new Department Name</span> 
			
			<button type="submit" class="btn">Submit</button>
		</fieldset>
	</form>
</div>
