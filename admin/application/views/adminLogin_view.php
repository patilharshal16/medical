<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
	<title>Admin</title>
</head>
<body>
	<form action="<?php echo base_url();?>adminlogin/verify" method="post" class="form-horizontal well">
		<table class="table">
			<tr class="info">
				<td>Username</td>
				<td>:</td>
				<td><input type="text" id="username" name="username" placeholder="Enter Username" class="span6"> </td>
			</tr>
			<tr class="info">
				<td>Password</td>
				<td>:</td>
				<td><input type="password" id="password" name="password" placeholder="********************" class="span6"> </td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><input type="submit" id="submit" name="submit" class="btn btn-success"> </td>
			</tr>
		</table>
	</form>
</body>
</html>
