<?php
class Report_model extends CI_Model{
	public function addReport($report,$dept){
		
		$data = array(
				'report_name' => $report,
				'dept_id' => $dept
				);
		$add=$this->db->insert('report',$data);
		if ($add){
			header("Location:".base_url()."report");
		}
	}
	
	public function getReport(){
		
		$this->db->select('*');
		$this->db->from('report');
		$this->db->join('department','report.dept_id = department.dept_id');
		$result=$this->db->get();
		$data = $result->result();
		return $data;
	}
	
	public function deleteReport($id){
		$delete=$this->db->delete('report','report_id ='.$id);
		if($delete){
			header('Location:'.base_url().'report');
		}
	}
}