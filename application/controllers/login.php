<?php
class Login extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('login_model');
		$this->load->helper('url');
	}
	
	public function index(){
		$data = array();
		if(isset($_GET['register']) ) { 
			if($_GET['register'] == 'true'){
				$data['register'] = 'success';
			}
		} else {
			$data['register'] = 'fail';
		}
		session_start();
		if(isset($_SESSION['userId'])){
			$url = base_url() . "profile";
			header("Location: ".$url);
		}		
		$this->load->view('login_view',$data);
	}
	
	public function authenticate(){
		
		$userId = $this->login_model->authenticate($_POST['username'],$_POST['password']);
		
		if($userId == 0) {
			echo "Incorrect Username or Password";
		} else {
			$data=$this->login_model->confirmLogin($userId);
			//echo $data;die();
			
			if($data == 1){
			$url = base_url() . "profile";
			header("Location: ".$url);
			}else {
				header("Location:".base_url()."profile/confirmProfile");
			}
		}
		
	}
	
	function logout(){
		session_start();
		session_destroy();
		header("Location:".base_url());
	}
}