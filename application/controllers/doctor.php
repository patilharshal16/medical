<?php 
class Doctor extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('doctor_model');
		$this->load->helper('url');
	}
	
	public function index(){
		
		$this->load->view('doctorLogin_view');
	}
	
	public function authenticate(){
	
		$doctorId = $this->doctor_model->authenticate($_POST['email'],$_POST['password']);
		if($doctorId == 0) {
			echo "Incorrect Username or Password";
		} else {
			$data=$this->doctor_model->confirmLogin($doctorId);
			if($data == 1){
				$url = base_url() . "doctor/welcome?id=".$doctorId;
				header("Location: ".$url);
			}else {
				header("Location:".base_url()."doctor/confirmRegistration");
			}
		}
	}
	
	public function welcome(){
		
		$this->load->view('home_view');
		$this->load->view('footer_view');
	}
	
	public function confirmRegistration(){
		$this->load->view('userConfirm_view');
	}
}