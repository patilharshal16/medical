<?php
class History extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('profile_model');
		$this->load->model('history_model');
		$this->load->view('header_view');
	}
	
	public function index(){
		$this->checkSession();
		$id= $_SESSION['userId'];
		$data = $this->history_model->getHistory($id);
		$this->load->view('history_view',array('data' => $data));
		$this->load->view('right-menu');
	}

	public function add(){
		$this->checkSession();
		

		$this->load->model('department_model');
		$data = $this->department_model->getDepts();
		$this->load->view('addHistory_view',array('data' => $data));
		$this->load->view('right-menu');
		$this->load->view('footer_view');
	}

	public function addHistory(){
		$this->checkSession();

		//$this->load->view('header_view');
		if (file_exists("history_files/" . $_FILES["historyDocument"]["name"]))
		{
			echo $_FILES["historyDocument"]["name"] . " already exists. ";
			die();
		}
		else
		{
			move_uploaded_file($_FILES["historyDocument"]["tmp_name"],"history_files/" . $_FILES["historyDocument"]["name"]);
			$file="history_files/" . $_FILES["historyDocument"]["name"];
		}

		$this->history_model->addHistory($_POST,$file);
		$this->load->view('footer_view');
	}
	
	public function delete(){
		$this->checkSession();
		
		unlink($_GET['file']);
		$this->history_model->deleteHistory($_GET['id']);
	}
	
	function checkSession(){
		session_start();
		if(!isset($_SESSION['userId'])){
			header("Location: ". base_url());
			die();
		}
	}
}