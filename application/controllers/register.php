<?php
class Register extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('register_model');
	}
	
	public function index(){
		$this->load->view('register_view');
		
		//$this->load->view('right-menu');
	}
	
	public function add_user(){
		
		$data=array(
				'username'=> $_POST['username'],
				'password'=> md5($_POST['password'])
		);
		
		$id = $this->register_model->add_user($data);
		
		header("Location: ". base_url()."register/confirm?id=".$id);
	}
	
	public function add_doctor(){
		$data = array(
				'name' => $_POST['name'],
				'email' => $_POST['email'],
				'password' => md5($_POST['password']),
				'specialist' => $_POST['specialist'],
				'graduation_college' => $_POST['graduation_college'],
				'graduation_year' => $_POST['graduation_Year'],
				'graduation_degree' => $_POST['degree'],
				'licence_no' => $_POST['license_number'],
				'state' => $_POST['state']
		);
		$did = $this->register_model->register_doctor($data);
		header("Location: ". base_url()."register/confirm?id=".$did);
	}
	
	public function doctor(){
		$this->load->view('registerDoctor_view');
	}
	
	public function confirm(){
		
		$this->load->view('confirm_view');
	}
	
	public function confirmDoctor(){
		$rno = $_GET['rno'];
		$doctorid = $this->register_model->confirmdoctor($rno);
	
		if($doctorid > 0){
			$loginUrl = base_url() . "login?register=success";
			header("Location: ".$loginUrl);
			die();
				
		}else {
			echo "invalid link";
		}
	}
	
	public function confirmUser(){
		$rno = $_GET['rno'];		
		$userId = $this->register_model->confirmUser($rno);
		
		if($userId > 0){
			$loginUrl = base_url() . "login?register=success";
			header("Location: ".$loginUrl);
			
		}else {
			echo "invalid link";
		}
	}
	
}
?>