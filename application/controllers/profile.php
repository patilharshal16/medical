<?php
class Profile extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('profile_model');
		
	}
	public function index(){
		$this->chechkSession();
		
		$data=$this->profile_model->getProfile($_SESSION['userId']);
		$fill=$this->profile_model->calculatePercentage($_SESSION['userId']);
		$viewdata = array();		
		$viewdata['data'] = $data;
		$viewdata['fill'] = $fill;
		$this->load->view('header_view');
		$this->load->view('profile_view',$viewdata);
		$this->load->view('right-menu');
	}
	
	public function confirmProfile(){
		$this->load->view('userConfirm_view');
	}

	public function saveabout(){
		$this->chechkSession();
		$this->profile_model->addabout($_POST);
	}

	public function savecontact(){
		$this->chechkSession();
		$this->profile_model->addcontact($_POST);
	}
	
	public function saveinsurance(){
		$this->chechkSession();
		$this->profile_model->addinsurance($_POST);
	}

	public function savemedication(){
		$this->chechkSession();
		$this->profile_model->addmedication($_POST);
	}

	public function saveallergy(){
		$this->chechkSession();
		$this->profile_model->addallergy($_POST);
	}
	
	public function savesymptom(){
		$this->chechkSession();
		$this->profile_model->addSymptom($_POST);
	}
	
	public function saveProcedure(){
		$this->chechkSession();
		$this->profile_model->addproc($_POST);
	}

	public function saveimmunization(){
		$this->chechkSession();
		$this->profile_model->addimmunization($_POST);
	}
 
	public function savepharmacy(){
		$this->chechkSession();
		$this->profile_model->addpharmacy($_POST);
	}
	
	public function getPerc(){
		$this->chechkSession();
		echo $this->profile_model->calculatePercentage($_POST['userId']);
	}
	
	function chechkSession(){
		session_start();
		if(!isset($_SESSION['userId'])){
			header("Location: ". base_url());
		}
	}

}