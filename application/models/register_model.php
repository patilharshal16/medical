<?php
class Register_model extends CI_Model {
	function add_user($data){
		$code = md5(rand(1000,9999));
		$url = base_url() . "register/confirmUser?rno=".$code;
		
		$data['confirmation_code']=$code;
		$this->db->insert('login',$data);
		$id = $this->db->insert_id();
		$this->sendMail($data,$url);
		return $id;
	}
	
	function sendMail($data, $url){
		$email = $data['username'];
		$content = "Congrats! You are now registered on MyMedicalDiary. Click on the following link to confirm your registration.  ";
		$content .= $url;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		mail($email,"Welcome to MyMedicalDiary!",$content,$headers);
	}
	
	function confirmUser($rno){
		
		$this->db->select('userid');
		$this->db->where('confirmation_code',$rno);
		$query = $this->db->get('login');
		
		if($query->result()) {
			$result = $query->result();
			$userId = $result[0]->userid;
			
			$data = array('confirmation_code' => '1');
			$this->db->where('userid',$userId); 
			$this->db->update('login',$data);
			
		} else {
			echo "Incorrect link. Please check again";
			return 0;
		}
		return $userId;
	}
	
	function register_doctor($data){
		$code = md5(rand(1000,9999));
		$url = base_url() . "register/confirmDoctor?rno=".$code;
		$data['confirmation_code']=$code;
		$this->db->insert('doctor',$data);
		$id = $this->db->insert_id();
		$this->confirmMail($data,$url);
		return $id;
	}
	
	function confirmMail($data, $url){
		$email = $data['email'];
		$content = "Congrats! You are now registered on MyMedicalDiary. Click on the following link to confirm your registration.  ";
		$content .= $url;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		mail($email,"Welcome to MyMedicalDiary!",$content,$headers);
	}
	
	function confirmdoctor($rno){
		$this->db->select('id');
		$this->db->where('confirmation_code',$rno);
		$query = $this->db->get('doctor');
		$doctorid = "";
		if($query->result()) {
			$result = $query->result();
			$doctorid = $result[0]->id;
				
			$data = array('confirmation_code' => '1');
			$this->db->where('id',$doctorid);
			$this->db->update('doctor',$data);
				
		} else {
			echo "Incorrect link. Please check again";
			return 0;
		}
		return $doctorid;
	}
	
}