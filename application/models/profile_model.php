<?php
class profile_model extends CI_Model{

	public function addabout($values){
		$data = array(
				'userid' => $values['userId'],
				'fname'=> $values['fname'],
				'lname'=> $values['lname'],
				'sex'=> $values['sex'],
				'birthday' => $values['birthday'],
				'location' => $values['location'],
				'zip' => $values['zip'],
				'photo' => $values['photo'],
				'height' => $values['height1'] .".". $values['height2'],
				'weight' => $values['weight'],
				'bloodgroup' => $values['bloodgroup']
		);
		$data['aboutscore'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$values['userId']);
		$sql=$this->db->get('aboutme');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('aboutme',$data);
		}else{
			$this->db->where(array('userid' => $values['userId']));
			$this->db->update('aboutme', $data);
		}
		echo "saved";
	}

	public function addcontact($values){

		$data = array(
				'userid' => $values['userId'],
				'address1' => $values['address1'],
				'address2' => $values['address2'],
				'city' => $values['city'],
				'state' => $values['state'],
				'contact_zip' => $values['zip'],
				'phoneno' => $values['phoneno'],
				'emergencyname' => $values['emergencyname'],
				'emergencyrelation' => $values['emergencyrelation'],
				'emergencyphone' => $values['emergencyphone'],
				'emergencyaddress1' => $values['emergencyaddress1'],
				'emergencyaddress2' => $values['emergencyaddress2'],
				'emergencycity' => $values['emergencycity'],
				'emergencystate' => $values['emergencystate'],
				'emergencyzip' => $values['emergencyzip']
		);

		$data['score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$values['userId']);
		$sql=$this->db->get('contactinfo');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('contactinfo',$data);
		}else{
			$this->db->where(array('userid' => $values['userId']));
			$this->db->update('contactinfo', $data);
		}

		echo "saved";
	}

	public function addinsurance($value){
		$data = array(
				'userid' => $value['userId'],
				'insurenceprovider'=> $value['insurenceprovider'],
				'policyholdername'=> $value['policyholdername'],
				'policyholderbirthdate'=> $value['policyholderbirthdate'],
				'policy' => $value['policy'],
				'group' => $value['group'],
				'employername' => $value['employername'],
				'employerworkno' => $value['employerworkno'],
				'employeraddress1' => $value['employeraddress1'],
				'employeraddress2' => $value['employeraddress2'],
				'employercity' => $value['employercity'],
				'employerstate' => $value['employerstate'],
				'employerzip' => $value['employerzip']
		);
		$data['insurenceinfo_score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$value['userId']);
		$sql=$this->db->get('insurenceinfo');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('insurenceinfo',$data);
		}else{
			$this->db->where(array('userid' => $value['userId']));
			$this->db->update('insurenceinfo', $data);
		}
		echo "saved";
	}

	public function addmedication($value){
		$data = array(
				'userid' => $value['userId'],
				'medication'=> $value['medication'],
				'medication_notes'=> $value['notes'],
				'currentlytaking'=> $value['stilltaking']
		);
		$data['medication_score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$value['userId']);
		$sql=$this->db->get('medication');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('medication',$data);
		}else{
			$this->db->where(array('userid' => $value['userId']));
			$this->db->update('medication', $data);
		}
		echo "saved";
	}

	public function addallergy($values){
		$data = array(
				'userid' => $values['userId'],
				'allergy'=> $values['allergy'],
				'allergy_notes'=> $values['notes'],
				'currentllhaving'=> $values['currentllhaving']
		);
		$data['allergy_score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where(array('userid' => $values['userId']));
		$sql=$this->db->get('allergy');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('allergy',$data);
		}else{
			$this->db->where(array('userid' => $values['userId']));
			$this->db->update('allergy', $data);
		}
		echo "saved";
	}

	public function addSymptom($values){

		$data = array(
	 		'userid' => $values['userId'],
	 		'symptomname'=> $values['symptom'],
	 		'symptoms_notes'=> $values['notes'],
	 		'currentlyhaving'=> $values['having']
		);
		$data['symptoms_score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$values['userId']);
		$sql=$this->db->get('symptoms');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('symptoms',$data);
		}else{
			$this->db->where(array('userid' => $values['userId']));
			$this->db->update('symptoms', $data);
		}
		echo "saved";
	}

	public function addproc($values){
		$data = array(
				'userid' => $values['userId'],
				'procedure'=> $values['name'],
				'procedure_notes'=> $values['notes'],
				'procedure_date'=> $values['date']
		);
		$data['procedure_score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$values['userId']);
		$sql=$this->db->get('procedure');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('procedure',$data);
		}else{
			$this->db->where(array('userid' => $values['userId']));
			$this->db->update('procedure', $data);
		}
		echo "Saved";
	}

	public function addimmunization($values){
		$data = array(
				'userid' => $values['userId'],
				'immunization'=> $values['name'],
				'immunization_notes'=> $values['notes'],
				'immunization_date'=> $values['date']
		);
		$data['immunization_score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$values['userId']);
		$sql=$this->db->get('immunization');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('immunization',$data);
		}else{
			$this->db->where(array('userid' => $values['userId']));
			$this->db->update('immunization', $data);
		}
		echo "Saved";
	}

	public function addpharmacy($values){
		$data = array(
				'userid' => $values['userId'],
				'pharmacyname' => $values['name'],
				'pharmacyfax' => $values['fax'],
				'pharmacyphone' => $values['phone'],
				'orderpharmacy' => $values['mail'],
				'orderfax' => $values['order_fax'],
				'orderphone' =>$values['order_phone']
		);
		$data['pharmacy_score'] = $this->getScore($data);
		$this->db->select('*');
		$this->db->where('userid',$values['userId']);
		$sql=$this->db->get('pharmacy');
		$result=$sql->num_rows();

		if ($result==0){
			$this->db->insert('pharmacy',$data);
		}else{
			$this->db->where(array('userid' => $values['userId']));
			$this->db->update('pharmacy', $data);
		}
		echo "Saved";
	}

	public function getProfile($userid){
		$count = 0;
		$row = array();
		$this->db->select('*');
		$this->db->from('aboutme');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}

		$this->db->select('*');
		$this->db->from('contactinfo');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		$this->db->select('*');
		$this->db->from('insurenceinfo');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		$this->db->select('*');
		$this->db->from('medication');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		$this->db->select('*');
		$this->db->from('allergy');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		$this->db->select('*');
		$this->db->from('symptoms');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		$this->db->select('*');
		$this->db->from('procedure');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		$this->db->select('*');
		$this->db->from('immunization');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		$this->db->select('*');
		$this->db->from('pharmacy');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$row[$key] = $value;
		}
		return $row;
		
	}

	public function getScore($data){
		$total=count($data);
		$result=$total;
		foreach ($data as $value){
			if ($value==""){
				$result--;
			}
		}
		return ($result/$total);
	}

	public function calculatePercentage($userid){

		$count = 0;
		$this->db->select('aboutscore');
		$this->db->from('aboutme');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			
			$count = $count+$value;
		}
		
		$this->db->select('score');
		$this->db->from('contactinfo');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		$this->db->select('insurenceinfo_score');
		$this->db->from('insurenceinfo');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		$this->db->select('medication_score');
		$this->db->from('medication');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		$this->db->select('allergy_score');
		$this->db->from('allergy');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		$this->db->select('symptoms_score');
		$this->db->from('symptoms');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		$this->db->select('procedure_score');
		$this->db->from('procedure');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		$this->db->select('immunization_score');
		$this->db->from('immunization');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		$this->db->select('pharmacy_score');
		$this->db->from('pharmacy');
		$this->db->where(array('userid' => $userid));
		$select=$this->db->get();
		$results = $select->result_array();
		if($results)
			foreach($results[0] as $key=>$value){
			$count = $count+$value;
		}
		
		return ($count/9)*100;		
	}
}