<?php
class Department_model extends CI_Model{
	
	public function addDept($dept){
		$data = array(
				'departmentName' => $dept	
				);
		$result=$this->db->insert('department',$data);
		if ($result){
			header("Location:".base_url()."department");
		}
	}
	
	public function getDepts(){
		$result = $this->db->get('department');
		$values=$result->result();
		return $values;
	}
	
	public function deleteDept($id){
		$tables = array('report','department');
		$this->db->where('dept_id', $id);
		$this->db->delete($tables);
		$delete = $this->db->affected_rows();
		if ($delete){
			header('Location:'.base_url().'department');
		}
		else {
			echo 'Department can not be Deleted';
		}
	}
}