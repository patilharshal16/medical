<?php
class Login_model extends CI_Model {
	function authenticate($userName,$password){

		$this->db->select('userid');
		$this->db->where(array('username' => $userName, 'password' => md5($password)));
		$query = $this->db->get('login');
		
		if($query->result()) {
			$result = $query->result();
			$userId = $result[0]->userid;
			session_start();
			$_SESSION['userId'] = $userId;
			return $userId;
		} else {
			return 0;
		}
	}
	
	public function confirmLogin($userid){
		
		$this->db->select('*');
		$this->db->where(array('userid' =>$userid,'confirmation_code' => 1));
		$query = $this->db->get('login');
		$numRows = $query->num_rows();
		return $numRows;
	}
}