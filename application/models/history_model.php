<?php
class history_model extends CI_Model{
	
	public function addHistory($values,$doc){
		session_start();
				
		$data = array(
				'userid' => $_SESSION['userId'],
				'history_name' => $values['historyName'],
				'dept_id' => $values['department'],
				'treatment_date' => $values['treatmentDate'],
				'hospital_name' => $values['hospitalName'],
				'doctor_name' => $values['doctorName'],
				'history_document' => $doc,
				);
		$insert = $this->db->insert('history',$data);
		if ($insert){
			header("Location:".base_url()."history");
		}
	}
	
	public function getHistory($id){

		$this->db->select('*');
		$this->db->from('history');
		$this->db->where('userid ='.$id);
		$this->db->join('department','history.dept_id = department.dept_id');
		$result=$this->db->get();
		$data = $result->result();
		return $data;
	}
	
	public function deleteHistory($id){
		
		$this->db->where('id', $id);
		$this->db->delete('history');
		$delete = $this->db->affected_rows();
		if ($delete){
			header('Location:'.base_url().'history');
		}
		else {
			echo 'Record can not be Deleted';
		}
	}
}