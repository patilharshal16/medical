<?php
class doctor_model extends CI_Model{
	function authenticate($userName,$password){
	
		$this->db->select('id');
		$this->db->where(array('email' => $userName, 'password' => md5($password)));
		$query = $this->db->get('doctor');
	
		if($query->result()) {
			$result = $query->result();
			$doctorid = $result[0]->id;
			session_start();
			$_SESSION['id'] = $doctorid;
			return $doctorid;
		} else {
			return 0;
		}
	}
	
	public function confirmLogin($userid){
	
		$this->db->select('*');
		$this->db->where(array('id' =>$userid,'confirmation_code' => 1));
		$query = $this->db->get('doctor');
		$numRows = $query->num_rows();
		return $numRows;
	}
}