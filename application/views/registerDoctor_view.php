<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="<?php echo base_url();?>styles/style.css" />
<title>Medical Diary</title>
</head>
<body>
	<!-- <div class="logo"></div> -->
	<div class="container">
		<table>
			<tr>
				<td><a id="user" href="<?php echo base_url();?>register">Member</a>
					<span class="or-divider">or</span> <a
					href="<?php echo base_url();?>register/doctor" id="doctor">Doctor</a>
				</td>
			</tr>
		</table>
		<form action="<?php echo base_url();?>register/add_doctor" method="POST">
			<table width="100%">
				<tr>
					<td>Name</td>
					<td>:</td>
					<td><input type="text" name="name" id="name"
						placeholder="Your Name"></td>
				</tr>
				<tr>
					<td>Email Id</td>
					<td>:</td>
					<td><input type="text" name="email" id="email"
						placeholder="john@example.com" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td>:</td>
					<td><input type="password" name="password" id="password"
						placeholder="Choose a Password" /></td>
				</tr>
				<tr>
					<td>Specialist</td>
					<td>:</td>
					<td><select id="specialist" name="specialist">
							<option value="">Area of Practice</option>
							<option value="Addiction Medicine">Addiction Medicine</option>
							<option value="ADHD and Autism">ADHD &amp; Autism</option>
							<option value="Aerospace Medicine">Aerospace Medicine</option>
							<option value="Aesthetic Medicine">Aesthetic Medicine</option>
							<option value="Anesthesiology">Anesthesiology</option>
							<option value="Bariatrics">Bariatrics</option>
							<option value="Breast Surgery">Breast Surgery</option>
							<option value="Cardiac Electrophysiology">Cardiology - Cardiac
								Electrophysiology</option>
							<option value="Clinical Genetics">Clinical Genetics</option>
							<option value="Clinical Psychology">Clinical Psychology</option>
							<option value="Critical Care">Critical Care</option>
							<option value="Dentistry">Dentistry</option>
							<option value="Cosmetic Dentistry">Dentistry - Cosmetic</option>
							<option value="Endodontics">Dentistry - Endodontics</option>
							<option value="Orthodontics">Dentistry - Orthodontics</option>
							<option value="Pediatric Dentistry">Dentistry - Pediatric</option>
							<option value="Periodontics">Dentistry - Periodontics</option>
							<option value="Prosthodontics">Dentistry - Prosthodontics</option>
							<option value="Dermatology">Dermatology</option>
							<option value="Emergency Medicine">Emergency Medicine</option>
							<option value="ENT and Head and Neck Surgery">ENT - Head &amp;
								Neck Surgery</option>
							<option value="Pediatric ENT and Head and Neck Surgery">ENT -
								Head &amp; Neck Surgery - Pediatric</option>
							<option value="Environmental Health">Environmental Health</option>
							<option value="Facial Plastic Surgery">Facial Plastic Surgery</option>
							<option value="Family Medicselected">Family Medicine</option>
							<option value="Fertility Medicine">Fertility Medicine</option>
							<option value="General Practice">General Practice</option>
							<option value="Genetics Counseling">Genetics Counseling</option>
							<option value="Gynecology">Gynecology</option>
							<option value="Gynecologic Oncology">Gynecology - Oncology</option>
							<option value="Hair Restoration">Hair Restoration</option>
							<option value="Head &amp; Neck Surgery">Head &amp; Neck Surgery</option>
							<option value="Holistic Medicine">Holistic Medicine</option>
							<option value="Internal Medicine">Internal Medicine</option>
							<option value="Internal Medicine and Pediatrics">Internal
								Medicine &amp; Pediatrics</option>
							<option value="Allergy">Internal Medicine - Allergy</option>
							<option value="Allergy and Immunology">Internal Medicine -
								Allergy &amp; Immunology</option>
							<option value="Cardiology">Internal Medicine - Cardiology</option>
							<option value="Diabetology">Internal Medicine - Diabetology</option>
							<option value="Endocrinology">Internal Medicine - Endocrinology</option>
							<option value="Gastroenterology">Internal Medicine -
								Gastroenterology</option>
							<option value="Geriatrics">Internal Medicine - Geriatrics</option>
							<option value="Hematology">Internal Medicine - Hematology</option>
							<option value="Hematology and Oncology">Internal Medicine -
								Hematology &amp; Oncology</option>
							<option value="Hepatology">Internal Medicine - Hepatology</option>
							<option value="Hospital-based practice">Internal Medicine -
								Hospital-based practice</option>
							<option value="Immunology">Internal Medicine - Immunology</option>
							<option value="Infectious Disease">Internal Medicine - Infectious
								Disease</option>
							<option value="Nephrology and Dialysis">Internal Medicine -
								Nephrology &amp; Dialysis</option>
							<option value="Obstetric Medicine">Internal Medicine - Obstetric
								Medicine</option>
							<option value="Medical Oncology">Internal Medicine - Oncology</option>
							<option value="Pulmonary Critical Care">Internal Medicine -
								Pulmonary Critical Care</option>
							<option value="Pulmonology">Internal Medicine - Pulmonology</option>
							<option value="Rheumatology">Internal Medicine - Rheumatology</option>
							<option value="Sleep Medicine">Internal Medicine - Sleep Medicine</option>
							<option value="Neurology">Neurology</option>
							<option value="Neurosurgery">Neurosurgery</option>
							<option value="Nuclear Medicine">Nuclear Medicine</option>
							<option value="Obstetrics and Gynecology">Obstetrics &amp;
								Gynecology</option>
							<option value="Maternal-Fetal Medicine">Obstetrics &amp;
								Gynecology - Maternal Fetal Medicine</option>
							<option value="Urogynecology">Obstetrics &amp; Gynecology -
								Urogynecology</option>
							<option value="Occupational Medicine">Occupational Medicine</option>
							<option value="Ophthalmology">Ophthalmology</option>
							<option value="LASIK Surgery">Ophthalmology - LASIK Surgery</option>
							<option value="Pediatric Ophthalmology">Ophthalmology - Pediatric</option>
							<option value="Retinal Surgery">Ophthalmology - Retinal Surgery</option>
							<option value="Orthopedic Surgery">Orthopedic Surgery</option>
							<option value="Orthopedic Foot and Ankle Surgery">Orthopedic
								Surgery - Foot &amp; Ankle</option>
							<option value="Pediatric Orthopedic Surgery">Orthopedic Surgery -
								Pediatric</option>
							<option value="Orthopedic Reconstructive Surgery">Orthopedic
								Surgery - Reconstruction</option>
							<option value="Orthopedic Spine Surgery">Orthopedic Surgery -
								Spine</option>
							<option value="Pain Management">Pain Management</option>
							<option value="Palliative Care">Palliative Care</option>
							<option value="Pathology">Pathology</option>
							<option value="Pediatric Rehabilitation Medicine">Pediatric
								Rehabilitation Medicine</option>
							<option value="Pediatric Rheumatology">Pediatric Rheumatology</option>
							<option value="Pediatrics">Pediatrics</option>
							<option value="Adolescent Medicine">Pediatrics - Adolescent
								Medicine</option>
							<option value="Pediatric Allergy">Pediatrics - Allergy</option>
							<option value="Pediatric Allergy and Asthma">Pediatrics - Allergy
								&amp; Asthma</option>
							<option value="Pediatric Cardiology">Pediatrics - Cardiology</option>
							<option value="Pediatric Critical Care">Pediatrics - Critical
								Care</option>
							<option value="Pediatric Dermatology">Pediatrics - Dermatology</option>
							<option value="Developmental and Behavioral Pediatrics">Pediatrics
								- Developmental &amp; Behavioral</option>
							<option value="Pediatric Emergency Medicine">Pediatrics -
								Emergency Medicine</option>
							<option value="Pediatric Endocrinology">Pediatrics -
								Endocrinology</option>
							<option value="Pediatric Gastroenterology">Pediatrics -
								Gastroenterology</option>
							<option value="Pediatric Hematology and Oncology">Pediatrics -
								Hematology &amp; Oncology</option>
							<option value="Pediatric Infectious Disease">Pediatrics -
								Infectious Disease</option>
							<option value="Neonatology">Pediatrics - Neonatology</option>
							<option value="Pediatric Nephrology and Dialysis">Pediatrics -
								Nephrology &amp; Dialysis</option>
							<option value="Pediatric Neurology">Pediatrics - Neurology</option>
							<option value="Pediatric Oncology">Pediatrics - Oncology</option>
							<option value="Child Psychiatry">Pediatrics - Psychiatry</option>
							<option value="Pediatric Pulmonology">Pediatrics - Pulmonology</option>
							<option value="Pediatric Sports Medicine">Pediatrics - Sports
								Medicine</option>
							<option value="Pediatric Urology">Pediatrics - Urology</option>
							<option value="Pharmacology">Pharmacology</option>
							<option value="Phlebology">Phlebology</option>
							<option value="Physical Medicine and Rehabilitation">Physical
								&amp; Rehabilitation Medicine</option>
							<option value="Physical and Rehabilitation Medicine">Physical and
								Rehabilitation Medicine</option>
							<option value="Podiatry">Podiatry</option>
							<option value="Preventive Medicine">Preventive Medicine</option>
							<option value="Psychiatry">Psychiatry</option>
							<option value="Geriatric Psychiatry">Psychiatry - Geriatric</option>
							<option value="Public Health">Public Health</option>
							<option value="Radiation Oncology">Radiation Oncology</option>
							<option value="Radiology">Radiology</option>
							<option value="Interventional Radiology">Radiology -
								Interventional</option>
							<option value="Sports Medicine">Sports Medicine</option>
							<option value="General Surgery">Surgery</option>
							<option value="Colon and Rectal Surgery">Surgery - Colorectal</option>
							<option value="Hand Surgery">Surgery - Hand Surgery</option>
							<option value="Head and Neck Surgery">Surgery - Head &amp; Neck</option>
							<option value="Surgical Oncology">Surgery - Oncology</option>
							<option value="Oral and Maxillofacial Surgery">Surgery - Oral
								&amp; Maxillofacial</option>
							<option value="Pediatric Surgery">Surgery - Pediatric</option>
							<option value="Plastic Surgery">Surgery - Plastics</option>
							<option value="Thoracic Surgery">Surgery - Thoracic</option>
							<option value="Transplant Surgery">Surgery - Transplant</option>
							<option value="Trauma Surgery">Surgery - Trauma</option>
							<option value="Vascular Surgery">Surgery - Vascular</option>
							<option value="Toxicology">Toxicology</option>
							<option value="Transfusion Medicine">Transfusion Medicine</option>
							<option value="Travel Medicine">Travel Medicine</option>
							<option value="Undersea and Hyperbaric Medicine">Undersea and
								Hyperbaric Medicine</option>
							<option value="Urgent Care">Urgent Care</option>
							<option value="Urology">Urology</option>
							<option value="Urologic Oncology">Urology - Oncology</option>
							<option value="Wilderness Medicine">Wilderness Medicine</option>
							<option value="Wound care">Wound care</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>Graduation College</td>
					<td>:</td>
					<td><input type="text" id="graduation_college"
						name="graduation_college" placeholder="Enter College name"></td>
				</tr>
				<tr>
					<td>Graduation Year</td>
					<td>:</td>
					<td><select name="graduation_Year" id="graduation_Year">
							<option>- Year -</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>
							<option value="2007">2007</option>
							<option value="2006">2006</option>
							<option value="2005">2005</option>
							<option value="2004">2004</option>
							<option value="2003">2003</option>
							<option value="2002">2002</option>
							<option value="2001">2001</option>
							<option value="2000">2000</option>
							<option value="1999">1999</option>
							<option value="1998">1998</option>
							<option value="1997">1997</option>
							<option value="1996">1996</option>
							<option value="1995">1995</option>
							<option value="1994">1994</option>
							<option value="1993">1993</option>
							<option value="1992">1992</option>
							<option value="1991">1991</option>
							<option value="1990">1990</option>
							<option value="1989">1989</option>
							<option value="1988">1988</option>
							<option value="1987">1987</option>
							<option value="1986">1986</option>
							<option value="1985">1985</option>
							<option value="1984">1984</option>
							<option value="1983">1983</option>
							<option value="1982">1982</option>
							<option value="1981">1981</option>
							<option value="1980">1980</option>
							<option value="1979">1979</option>
							<option value="1978">1978</option>
							<option value="1977">1977</option>
							<option value="1976">1976</option>
							<option value="1975">1975</option>
							<option value="1974">1974</option>
							<option value="1973">1973</option>
							<option value="1972">1972</option>
							<option value="1971">1971</option>
							<option value="1970">1970</option>
							<option value="1969">1969</option>
							<option value="1968">1968</option>
							<option value="1967">1967</option>
							<option value="1966">1966</option>
							<option value="1965">1965</option>
							<option value="1964">1964</option>
							<option value="1963">1963</option>
							<option value="1962">1962</option>
							<option value="1961">1961</option>
							<option value="1960">1960</option>
							<option value="1959">1959</option>
							<option value="1958">1958</option>
							<option value="1957">1957</option>
							<option value="1956">1956</option>
							<option value="1955">1955</option>
							<option value="1954">1954</option>
							<option value="1953">1953</option>
							<option value="1952">1952</option>
							<option value="1951">1951</option>
							<option value="1950">1950</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>Qualification</td>
					<td>:</td>
					<td><select id="degree" name="degree">
							<option value="">Qualifying Degree</option>
							<option value="MD">MD</option>
							<option value="MDCM">MDCM</option>
							<option value="DO">DO</option>
							<option value="MBBS">MBBS</option>
							<option value="MBChB">MBChB</option>
							<option value="DMD">DMD</option>
							<option value="DDS">DDS</option>
							<option value="DPM">DPM</option>
							<option value="EdD">EdD</option>
							<option value="PsyD">PsyD</option>
							<option value="PhD">PhD</option>
							<option value="PharmD">PharmD</option>
					</select></td>
				</tr>
				<tr>
					<td>Professional license number</td>
					<td>:</td>
					<td><input type="text" id="license_number" name="license_number" placeholder="Enter License number"></td>
				</tr>
				<tr>
					<td>State</td>
					<td>:</td>
					<td><select name="state" id="state">
							<option value="Andaman and Nicobar Islands">Andaman and Nicobar
								Islands</option>
							<option value="Andhra Pradesh">Andhra Pradesh</option>
							<option value="Arunachal Pradesh">Arunachal Pradesh</option>
							<option value="Assam">Assam</option>
							<option value="Bihar">Bihar</option>
							<option value="Chandigarh">Chandigarh</option>
							<option value="Chhattisgarh">Chhattisgarh</option>
							<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
							<option value="Daman and Diu">Daman and Diu</option>
							<option value="Delhi">Delhi</option>
							<option value="Goa">Goa</option>
							<option value="Gujarat">Gujarat</option>
							<option value="Haryana">Haryana</option>
							<option value="Himachal Pradesh">Himachal Pradesh</option>
							<option value="Jammu and Kashmir">Jammu and Kashmir</option>
							<option value="Jharkhand">Jharkhand</option>
							<option value="Karnataka">Karnataka</option>
							<option value="Kerala">Kerala</option>
							<option value="Lakshadweep">Lakshadweep</option>
							<option value="Madhya Pradesh">Madhya Pradesh</option>
							<option value="Maharashtra">Maharashtra</option>
							<option value="Manipur">Manipur</option>
							<option value="Meghalaya">Meghalaya</option>
							<option value="Mizoram">Mizoram</option>
							<option value="Nagaland">Nagaland</option>
							<option value="Orissa">Orissa</option>
							<option value="Pondicherry">Pondicherry</option>
							<option value="Punjab">Punjab</option>
							<option value="Rajasthan">Rajasthan</option>
							<option value="Sikkim">Sikkim</option>
							<option value="Tamil Nadu">Tamil Nadu</option>
							<option value="Tripura">Tripura</option>
							<option value="Uttaranchal">Uttaranchal</option>
							<option value="Uttar Pradesh">Uttar Pradesh</option>
							<option value="West Bengal">West Bengal</option>
					</select></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="submit" value="Sign Up" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>
