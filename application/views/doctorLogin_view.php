<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="styles/style.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/save.js"></script>
<title>Medical Diary</title>
</head>
<body>
	<div class="container">
		<table>
			<tr>
				<td><a id="user" href="<?php echo base_url();?>login">Member</a> 
				<span class="or-divider">or</span> 
				<a href="<?php echo base_url();?>doctor" id="doctor">Doctor</a></td>
			</tr>
		</table>
		<form action="doctor/authenticate" method="POST">
			<table width="100%">
				<tr>
					<td>Email</td>
					<td>:</td>
					<td><input type="text" name="email" id="email"
						placeholder="Your User Name" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td>:</td>
					<td><input type="password" name="password" id="password"
						placeholder="Your Password" /></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="submit" value="Login" /></td>
				</tr>
			</table>
		</form>

		<div class="message">
			Not a member yet? Register <a href="<?php base_url();?>register">here</a>.
		</div>

	</div>
</body>
</html>
