	<?php //print_r($data);die();?>
			<div id="inner">
				<!-- Left side menu tabs Start -->
				<div id="inner-tabs">
					<div id="inner-about" class="active btn-tab">
						<p>About Me</p>
					</div>
					<div id="inner-contact-info" class="btn-tab">
						<p>Contact Information</p>
					</div>
					<div id="inner-insurance-info" class="btn-tab">
						<p>Insurance Information</p>
					</div>
					<div id="inner-medications" class="btn-tab">
						<p>Medications</p>
					</div>
					<div id="inner-allergies" class="btn-tab">
						<p>Allergies</p>
					</div>
					<div id="inner-symptooms-cond" class="btn-tab">
						<p>Symptoms & Conditions</p>
					</div>
					<div id="inner-proc" class="btn-tab">
						<p>Procedures</p>
					</div>
					<div id="inner-immune" class="btn-tab">
						<p>Immunizations</p>
					</div>
					<div id="inner-pharma-info" class="btn-tab">
						<p>Pharmacy Information</p>
					</div>
				</div>
				<!-- Left side menu tabs Ends -->
				<div id="about-form" class="profile-forms">
					<form action="" method="post" enctype="multipart/form-data">
						<table>
							<tr>
								<td>First Name</td>
								<td>:</td>

								<td><input type="text" name="fname" id="fname"
									value="<?php if(isset($data['fname'])){echo $data['fname'];}else{ echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Last Name</td>
								<td>:</td>
								<td><input type="text" name="lname"
									value="<?php if(isset($data['lname'])){echo $data['lname'];}else{ echo "";}?>"
									placeholder="Enter Last Name" id="lname">
								</td>
							</tr>
							<tr>
								<td>Sex</td>
								<td>:</td>
								<td><select name="sex" id="sex">
										<option value="male">Male</option>
										<option value="female">Female</option>
								</select></td>
							</tr>
							<tr>
								<td>Born On</td>
								<td>:</td>
								<td><input type="date" name="birthday"
									value="<?php if(isset($data['birthday'])){echo $data['birthday'];}else{ echo "";}?>" id="birthday">
								</td>
							</tr>
							<tr>
								<td>Location</td>
								<td>:</td>
								<td><select name="location" id="location">
										<option value="Afghanistan">Afghanistan</option>
										<option value="Åland Islands">Åland Islands</option>
										<option value="Albania">Albania</option>
										<option value="Algeria">Algeria</option>
										<option value="American Samoa">American Samoa</option>
										<option value="Andorra">Andorra</option>
										<option value="Angola">Angola</option>
										<option value="Anguilla">Anguilla</option>
										<option value="Antarctica">Antarctica</option>
										<option value="Antigua and Barbuda">Antigua and Barbuda</option>
										<option value="Argentina">Argentina</option>
										<option value="Armenia">Armenia</option>
										<option value="Aruba">Aruba</option>
										<option value="Australia">Australia</option>
										<option value="Austria">Austria</option>
										<option value="Azerbaijan">Azerbaijan</option>
										<option value="Bahamas">Bahamas</option>
										<option value="Bahrain">Bahrain</option>
										<option value="Bangladesh">Bangladesh</option>
										<option value="Barbados">Barbados</option>
										<option value="Belarus">Belarus</option>
										<option value="Belgium">Belgium</option>
										<option value="Belize">Belize</option>
										<option value="Benin">Benin</option>
										<option value="Bermuda">Bermuda</option>
										<option value="Bhutan">Bhutan</option>
										<option value="Bolivia">Bolivia</option>
										<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
										<option value="Botswana">Botswana</option>
										<option value="Bouvet Island">Bouvet Island</option>
										<option value="Brazil">Brazil</option>
										<option value="British Indian Ocean Territory">British Indian
											Ocean Territory</option>
										<option value="Brunei Darussalam">Brunei Darussalam</option>
										<option value="Bulgaria">Bulgaria</option>
										<option value="Burkina Faso">Burkina Faso</option>
										<option value="Burundi">Burundi</option>
										<option value="Cambodia">Cambodia</option>
										<option value="Cameroon">Cameroon</option>
										<option value="Canada">Canada</option>
										<option value="Cape Verde">Cape Verde</option>
										<option value="Cayman Islands">Cayman Islands</option>
										<option value="Central African Republic">Central African
											Republic</option>
										<option value="Chad">Chad</option>
										<option value="Chile">Chile</option>
										<option value="China">China</option>
										<option value="Christmas Island">Christmas Island</option>
										<option value="Cocos (Keeling) Islands">Cocos (Keeling)
											Islands</option>
										<option value="Colombia">Colombia</option>
										<option value="Comoros">Comoros</option>
										<option value="Congo">Congo</option>
										<option value="Congo, The Democratic Republic of The">Congo,
											The Democratic Republic of The</option>
										<option value="Cook Islands">Cook Islands</option>
										<option value="Costa Rica">Costa Rica</option>
										<option value="Cote D'ivoire">Cote D'ivoire</option>
										<option value="Croatia">Croatia</option>
										<option value="Cuba">Cuba</option>
										<option value="Cyprus">Cyprus</option>
										<option value="Czech Republic">Czech Republic</option>
										<option value="Denmark">Denmark</option>
										<option value="Djibouti">Djibouti</option>
										<option value="Dominica">Dominica</option>
										<option value="Dominican Republic">Dominican Republic</option>
										<option value="Ecuador">Ecuador</option>
										<option value="Egypt">Egypt</option>
										<option value="El Salvador">El Salvador</option>
										<option value="Equatorial Guinea">Equatorial Guinea</option>
										<option value="Eritrea">Eritrea</option>
										<option value="Estonia">Estonia</option>
										<option value="Ethiopia">Ethiopia</option>
							     		<option value="Falkland Islands (Malvinas)">Falkland Islands
											(Malvinas)</option>
										<option value="Faroe Islands">Faroe Islands</option>
										<option value="Fiji">Fiji</option>
										<option value="Finland">Finland</option>
										<option value="France">France</option>
										<option value="French Guiana">French Guiana</option>
										<option value="French Polynesia">French Polynesia</option>
										<option value="French Southern Territories">French Southern
											Territories</option>
										<option value="Gabon">Gabon</option>
										<option value="Gambia">Gambia</option>
										<option value="Georgia">Georgia</option>
										<option value="Germany">Germany</option>
										<option value="Ghana">Ghana</option>
										<option value="Gibraltar">Gibraltar</option>
										<option value="Greece">Greece</option>
										<option value="Greenland">Greenland</option>
										<option value="Grenada">Grenada</option>
										<option value="Guadeloupe">Guadeloupe</option>
										<option value="Guam">Guam</option>
										<option value="Guatemala">Guatemala</option>
										<option value="Guernsey">Guernsey</option>
										<option value="Guinea">Guinea</option>
										<option value="Guinea-bissau">Guinea-bissau</option>
										<option value="Guyana">Guyana</option>
										<option value="Haiti">Haiti</option>
										<option value="Heard Island and Mcdonald Islands">Heard Island
											and Mcdonald Islands</option>
										<option value="Holy See (Vatican City State)">Holy See
											(Vatican City State)</option>
										<option value="Honduras">Honduras</option>
										<option value="Hong Kong">Hong Kong</option>
										<option value="Hungary">Hungary</option>
										<option value="Iceland">Iceland</option>
										<option value="India">India</option>
										<option value="Indonesia">Indonesia</option>
										<option value="Iran, Islamic Republic of">Iran, Islamic
											Republic of</option>
										<option value="Iraq">Iraq</option>
										<option value="Ireland">Ireland</option>
										<option value="Isle of Man">Isle of Man</option>
										<option value="Israel">Israel</option>
										<option value="Italy">Italy</option>
										<option value="Jamaica">Jamaica</option>
										<option value="Japan">Japan</option>
										<option value="Jersey">Jersey</option>
										<option value="Jordan">Jordan</option>
										<option value="Kazakhstan">Kazakhstan</option>
										<option value="Kenya">Kenya</option>
										<option value="Kiribati">Kiribati</option>
										<option value="Korea, Democratic People's Republic of">Korea,
											Democratic People's Republic of</option>
										<option value="Korea, Republic of">Korea, Republic of</option>
										<option value="Kuwait">Kuwait</option>
										<option value="Kyrgyzstan">Kyrgyzstan</option>
										<option value="Lao People's Democratic Republic">Lao People's
											Democratic Republic</option>
										<option value="Latvia">Latvia</option>
										<option value="Lebanon">Lebanon</option>
										<option value="Lesotho">Lesotho</option>
										<option value="Liberia">Liberia</option>
										<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
										<option value="Liechtenstein">Liechtenstein</option>
										<option value="Lithuania">Lithuania</option>
										<option value="Luxembourg">Luxembourg</option>
										<option value="Macao">Macao</option>
										<option value="Macedonia, The Former Yugoslav Republic of">Macedonia,
											The Former Yugoslav Republic of</option>
										<option value="Madagascar">Madagascar</option>
										<option value="Malawi">Malawi</option>
										<option value="Malaysia">Malaysia</option>
										<option value="Maldives">Maldivevalue</option>
										<option value="Mali">Mali</option>
										<option value="Malta">Malta</option>
										<option value="Marshall Islands">Marshall Islands</option>
										<option value="Martinique">Martinique</option>
										<option value="Mauritania">Mauritania</option>
										<option value="Mauritius">Mauritius</option>
										<option value="Mayotte">Mayotte</option>
										<option value="Mexico">Mexico</option>
										<option value="Micronesia, Federated States of">Micronesia,
											Federated States of</option>
										<option value="Moldova, Republic of">Moldova, Republic of</option>
										<option value="Monaco">Monaco</option>
										<option value="Mongolia">Mongolia</option>
										<option value="Montenegro">Montenegro</option>
										<option value="Montserrat">Montserrat</option>
										<option value="Morocco">Morocco</option>
										<option value="Mozambique">Mozambique</option>
										<option value="Myanmar">Myanmar</option>
									<option value="Namibia">Namibia</option>
										<option value="Nauru">Nauru</option>
										<option value="Nepal">Nepal</option>
										<option value="Netherlands">Netherlands</option>
										<option value="Netherlands Antilles">Netherlands Antilles</option>
										<option value="New Caledonia">New Caledonia</option>
										<option value="New Zealand">New Zealand</option>
										<option value="Nicaragua">Nicaragua</option>
										<option value="Niger">Niger</option>
										<option value="Nigeria">Nigeria</option>
										<option value="Niue">Niue</option>
										<option value="Norfolk Island">Norfolk Island</option>
										<option value="Northern Mariana Islands">Northern Mariana
											Islands</option>
										<option value="Norway">Norway</option>
										<option value="Oman">Oman</option>
										<option value="Pakistan">Pakistan</option>
										<option value="Palau">Palau</option>
										<option value="Palestinian Territory, Occupied">Palestinian
											Territory, Occupied</option>
										<option value="Panama">Panama</option>
										<option value="Papua New Guinea">Papua New Guinea</option>
										<option value="Paraguay">Paraguay</option>
										<option value="Peru">Peru</option>
										<option value="Philippines">Philippines</option>
										<option value="Pitcairn">Pitcairn</option>

										<option value="Poland">Poland</option>
										<option value="Portugal">Portugal</option>
										<option value="Puerto Rico">Puerto Rico</option>
										<option value="Qatar">Qatar</option>
										<option value="Reunion">Reunion</option>
										<option value="Romania">Romania</option>
										<option value="Russian Federation">Russian Federation</option>
										<option value="Rwanda">Rwanda</option>
										<option value="Saint Helena">Saint Helena</option>
										<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
										<option value="Saint Lucia">Saint Lucia</option>
										<option value="Saint Pierre and Miquelon">Saint Pierre and
											Miquelon</option>
										<option value="Saint Vincent and The Grenadines">Saint Vincent
											and The Grenadines</option>
										<option value="Samoa">Samoa</option>
										<option value="San Marino">San Marino</option>
										<option value="Sao Tome and Principe">Sao Tome and Principe</option>
										<option value="Saudi Arabia">Saudi Arabia</option>
										<option value="Senegal">Senegal</option>
										<option value="Serbia">Serbia</option>
										<option value="Seychelles">Seychelles</option>
										<option value="Sierra Leone">Sierra Leone</option>
										<option value="Singapore">Singapore</option>
										<option value="Slovakia">Slovakia</option>
										<option value="Slovenia">Slovenia</option>
										<option value="Solomon Islands">Solomon Islands</option>
										<option value="Somalia">Somalia</option>
										<option value="South Africa">South Africa</option>
										<option value="South Georgia and The South Sandwich Islands">South
											Georgia and The South Sandwich Islands</option>
										<option value="Spain">Spain</option>
										<option value="Sri Lanka">Sri Lanka</option>
										<option value="Sudan">Sudan</option>
										<option value="Suriname">Suriname</option>
										<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
										<option value="Swaziland">Swaziland</option>
										<option value="Sweden">Sweden</option>
										<option value="Switzerland">Switzerland</option>
										<option value="Syrian Arab Republic">Syrian Arab Republic</option>
										<option value="Taiwan, Province of China">Taiwan, Province of
											China</option>
										<option value="Tajikistan">Tajikistan</option>
										<option value="Tanzania, United Republic of">Tanzania, United
											Republic of</option>
										<option value="Thailand">Thailand</option>
										<option value="Timor-leste">Timor-leste</option>
										<option value="Togo">Togo</option>
										<option value="Tokelau">Tokelau</option>
										<option value="Tonga">Tonga</option>
										<option value="Trinidad and Tobago">Trinidad and Tobago</option>
										<option value="Tunisia">Tunisia</option>
										<option value="Turkey">Turkey</option>
										<option value="Turkmenistan">Turkmenistan</option>
										<option value="Turks and Caicos Islands">Turks and Caicos
											Islands</option>
										<option value="Tuvalu">Tuvalu</option>
										<option value="Uganda">Uganda</option>
										<option value="Ukraine">Ukraine</option>
										<option value="United Arab Emirates">United Arab Emirates</option>
										<option value="United Kingdom">United Kingdom</option>
										<option value="United States">United States</option>
										<option value="United States Minor Outlying Islands">United
											States Minor Outlying Islands</option>
										<option value="Uruguay">Uruguay</option>
										<option value="Uzbekistan">Uzbekistan</option>
										<option value="Vanuatu">Vanuatu</option>
										<option value="Venezuela">Venezuela</option>
										<option value="Viet Nam">Viet Nam</option>
										<option value="Virgin Islands, British">Virgin Islands,
											British</option>
										<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
										<option value="Wallis and Futuna">Wallis and Futuna</option>
										<option value="Western Sahara">Western Sahara</option>
										<option value="Yemen">Yemen</option>
										<option value="Zambia">Z431ambia</option>
										<option value="Zimbabwe">Zimbabwe</option>
								</select></td>
							</tr>
							<tr>
								<td>Zip</td>
								<td>:</td>
								<td><input type="text" name="zip"
									value="<?php if(isset($data['zip'])){echo $data['zip'];}else{ echo "";}?>" id="zip"></td>
							</tr>
							<tr>
								<td>Profile Photo</td>
								<td>:</td>
								<td><input type="file" name="photo" id="photo">
								</td>
							</tr>
							<tr>
								<td>Height</td>
								<td>:</td>
								<td><input type="text" name="height" class="frst-half-tb"
									id="height1" value="<?php if (isset($data['height'])){ echo floor($data['height']);}else{ echo "";}?>">ft<input
									type="text" name="height" class="scnd-half-tb" id="height2"
									value="<?php if (isset($data['height'])){ echo ($data['height']-floor($data['height']))*10;}else {echo "";}?>" />in</td>
							
							
							<tr>
								<td>Weight</td>
								<td>:</td>
								<td><input type="text" name="weight" id="weight"
									value="<?php if(isset($data['weight'])){echo $data['weight'];}else{ echo "";}?>" />lbs</td>
							</tr>
							<tr>
								<td>Blood Group</td>
								<td>:</td>
								<td><select name="bloodgroup" id="bloodgroup">
										<option value="o+">O+</option>
										<option value="a+">A+</option>
										<option value="b+">B+</option>
										<option value="ab+">AB+</option>
										<option value="o-">O-</option>
										<option value="a-">A-</option>
										<option value="b-">B-</option>
										<option value="ab-">AB-</option>
								</select></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" value="save"
									id="submit_about"></input></td>
							</tr>
						</table>
					</form>
				</div>

				<div id="contact-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Address</td>
								<td>:</td>
								<td><input type="text" name="address1"
									placeholder="Street Address1" id="address1"
									value="<?php if(isset($data['address1'])){echo $data['address1'];}else{ echo "";}?>" />
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="text" name="address2"
									placeholder="Street Address2" id="address2"
									value="<?php if(isset($data['address2'])){echo $data['address2'];}else{ echo "";}?>" />
								</td>
							</tr>
							<tr>
								<td>City</td>
								<td>:</td>
								<td><input type="text" name="city" placeholder="Enter city"
									id="city" value="<?php if (isset($data['city'])){ echo $data['city'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>State</td>
								<td>:</td>
								<td><input type="text" name="state" id="state"
									placeholder="Enter State" value="<?php if (isset($data['state'])){ echo $data['state'];}else {echo "";}?>" />
								</td>
							</tr>
							<tr>
								<td>ZIP</td>
								<td>:</td>
								<td><input type="text" name="contact_zip"
									placeholder="Enter Zip" id="contact_zip"
									value="<?php if (isset($data['contact_zip'])){ echo $data['contact_zip'];}else{ echo "";}?>" /></td>
							</tr>
							<tr>
								<td>Phone Number</td>
								<td>:</td>
								<td><input type="text" name="number" placeholder="Enter Number"
									id="number" value="<?php if(isset($data['phoneno'])){echo $data['phoneno'];}else {echo "";}?>">
								</td>
							</tr>
							<!-- <tr>
								<td>Carrier</td>
								<td>:</td>
								<td><select name="carrier" id="carrier"><option>carrier</option>
								</select></td>
							</tr> -->
							<tr>
								<td><b>Emergency Contact</b>
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Full Name</td>
								<td>:</td>
								<td><input type="text" name="name" placeholder="Enter Full Name"
									id="name" value="<?php if(isset($data['emergencyname'])){ echo $data['emergencyname'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Relation</td>
								<td>:</td>
								<td><input type="text" name="relation" id="relation"
									placeholder="Relation with you"
									value="<?php if (isset($data['emergencyrelation'])){ echo $data['emergencyrelation'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Contact</td>
								<td>:</td>
								<td><input type="text" name="contact" placeholder="Enter Number"
									id="contact" value="<?php if (isset($data['emergencyphone'])){ echo $data['emergencyphone'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Street Address</td>
								<td>:</td>
								<td><input type="text" name="emergencyaddress1"
									placeholder="Enter Street Address1" id="emergencyaddress1"
									value="<?php if (isset($data['emergencyaddress1'])){ echo $data['emergencyaddress1'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td></td>
								<td>:</td>
								<td><input type="text" name="emergencyaddress2"
									placeholder="Enter Street Address2" id="emergencyaddress2"
									value="<?php  if (isset($data['emergencyaddress2'])){ echo $data['emergencyaddress2'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td>City</td>
								<td>:</td>
								<td><input type="text" name="emergencycity"
									placeholder="Enter city" id="emergencycity"
									value="<?php   if (isset($data['emergencycity'])){ echo $data['emergencycity'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>State</td>
								<td>:</td>
								<td><input type="text" name="emergencystate" id="emergencystate"
									value="<?php if (isset($data['emergencystate'])){ echo $data['emergencystate'];}else {echo "";}?>" />
								</td>
							</tr>
							<tr>
								<td>ZIP</td>
								<td>:</td>
								<td><input type="text" name="emergencyzip"
									placeholder="Enter Zip" id="emergencyzip"
									value="<?php if (isset($data['emergencyzip'])){ echo $data['emergencyzip'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" id="submit_contactinfo">
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div id="ins-info-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Insurance Provider</td>
								<td>:</td>
								<td><select name="provider" id="provider">
										<option>Select Insurance Provider</option>
										<option value="Bajaj">Bajaj</option>
										<option value="LIC">LIC</option>
										<option value="TATA">TATA</option>
										<option value="ICICI">ICICI</option>
										<option value="HDFC">HDFC</option>
								</select></td>
							</tr>
							<tr>
								<td><b>POLICY HOLDER</b>
								</td>
							</tr>
							<tr>
								<td>Name</td>
								<td>:</td>
								<td><input type="text" name="name" placeholder="Enter Name"
									id="policy-holder-name"
									value="<?php if (isset($data['policyholdername'])){ echo $data['policyholdername'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Select Birthdate</td>
								<td>:</td>
								<td><input type="date" name="bdate" id="bdate"
									value="<?php if (isset($data['policyholderbirthdate'])){ echo $data['policyholderbirthdate'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Policy or ID Number</td>
								<td>:</td>
								<td><input type="text" name="policy"
									placeholder="Enter Policy Number" id="policy"
									value="<?php if (isset($data['policy'])){ echo $data['policy'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Group or Account Number</td>
								<td>:</td>
								<td><input type="text" name="group"
									placeholder="Enter Group or Account Number" id="group"
									value="<?php if (isset($data['group'])){ echo $data['group'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td><b>EMPLOYER</b>
								</td>
							</tr>
							<tr>
								<td>Name</td>
								<td>:</td>
								<td><input type="text" name="ename" placeholder="Enter Name"
									id="ename" value="<?php if (isset($data['employername'])){ echo $data['employername'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Work Phone</td>
								<td>:</td>
								<td><input type="text" name="worknumber"
									placeholder="Enter Work Number" id="worknumber"
									value="<?php if (isset($data['employerworkno'])){ echo $data['employerworkno'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Street Address</td>
								<td>:</td>
								<td><input type="text" name="address1"
									placeholder="Enter Street Address1" id="empaddress1"
									value="<?php if (isset($data['employeraddress1'])){ echo $data['employeraddress1'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="text" name="address2"
									placeholder="Enter Street Address2" id="empaddress2"
									value="<?php if (isset($data['employeraddress2'])){ echo $data['employeraddress2'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td>City</td>
								<td>:</td>
								<td><input type="text" name="city" placeholder="Enter city"
									id="empcity" value="<?php if (isset($data['employercity'])){ echo $data['employercity'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>State</td>
								<td>:</td>
								<td><input type="text" name="state" id="empstate"
									value="<?php if (isset($data['employerstate'])){ echo $data['employerstate'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td>ZIP</td>
								<td>:</td>
								<td><input type="text" name="zip" placeholder="Enter Zip"
									id="empzip" value="<?php if (isset($data['employerzip'])){ echo $data['employerzip'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" id="submit_insuranceinfo">
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div id="medication-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Medication</td>
								<td>:</td>
								<td><input type="text" name="name" placeholder="Enter name"
									id="medication_name" value="<?php if (isset($data['medication'])){ echo $data['medication'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Notes</td>
								<td>:</td>
								<td><input type="text" name="notes"
									placeholder="Side effects, Complications, Dosage etc."
									id="notes" value="<?php if (isset($data['medication_notes'])){ echo $data['medication_notes'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="checkbox" name="stilltaking"
									value="StillTaking" id="stilltaking" />Still Taking</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" id="submit_medication">
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div id="allergies-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Allergy</td>
								<td>:</td>
								<td><input type="text" name="allergy"
									placeholder="Enter Allergy" id="allergy"
									value="<?php if (isset($data['allergy'])){ echo $data['allergy'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Notes</td>
								<td>:</td>
								<td><input type="text" name="notes"
									placeholder="Date of last reaction etc." id="allergy_notes"
									value="<?php if (isset($data['allergy_notes'])){ echo $data['allergy_notes'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="checkbox" name="nothaving" value="NotHaving"
									id="nothaving" />I No Longer Have This</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" id="submit_allergies">
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div id="sympt-cond-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Symptoms</td>
								<td>:</td>
								<td><input type="text" name="symptom"
									placeholder="Enter Allergy" id="symptom"
									value="<?php if (isset($data['symptomname'])){ echo $data['symptomname'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Notes</td>
								<td>:</td>
								<td><input type="text" name="notes"
									placeholder="Date of last reaction etc." id="symptom_notes"
									value="<?php if (isset($data['symptoms_notes'])){ echo $data['symptoms_notes'];}else {echo "";}?>" /></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="checkbox" name="nothaving" value="NotHaving"
									id="NotHaving" />I No Longer Have This</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" id="submit_symptcond">
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div id="proc-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Procedure</td>
								<td>:</td>
								<td><input type="text" name="name" placeholder="Enter Allergy"
									id="procedure_name" value="<?php if (isset($data['procedure'])){ echo $data['procedure'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Notes</td>
								<td>:</td>
								<td><input type="text" name="notes"
									placeholder="Side effects, Complications, Dosage etc."
									id="procedure_notes"
									value="<?php if (isset($data['procedure_notes'])){ echo $data['procedure_notes'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Date Performed</td>
								<td>:</td>
								<td><input type="date" name="date" id="procedure_date"
									value="<?php if (isset($data['procedure_date'])){ echo $data['procedure_date'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" id="submit_proc">
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div id="immune-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Immunization</td>
								<td>:</td>
								<td><input type="text" name="immunization"
									placeholder="Enter Allergy" id="immunization"
									value="<?php if (isset($data['immunization'])){ echo $data['immunization'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Notes</td>
								<td>:</td>
								<td><input type="text" name="notes" id="immunizationNotes"
									value="<?php if (isset($data['immunization_notes'])){ echo $data['immunization_notes'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td>Date Administered</td>
								<td>:</td>
								<td><input type="date" name="date" id="immunizationDate"
									value="<?php if (isset($data['immunization_date'])){ echo $data['immunization_date'];}else {echo "";}?>"></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><input type="submit" name="submit" id="submit_immune">
								</td>
							</tr>

						</table>
					</form>
				</div>

				<div id="pharm-info-form" class="profile-forms">
					<form action="" method="post">
						<table>
							<tr>
								<td>Pharmacy name:</td>
								<td><input type="text" name="pharmacyname" id="pharmacyname"
									value="<?php  if (isset($data['pharmacyname'])){ echo $data['pharmacyname'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>FAX</td>
								<td><input type="text" name="fax" id="fax"
									value="<?php  if (isset($data['pharmacyfax'])){ echo $data['pharmacyfax'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Phone</td>
								<td><input type="text" name="phone" id="phone"
									value="<?php  if (isset($data['pharmacyphone'])){ echo $data['pharmacyphone'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Mail-Order Pharmacy:</td>
								<td><input type="text" name="orderpharmacy" id="orderpharmacy"
									value="<?php  if (isset($data['orderpharmacy'])){ echo $data['orderpharmacy'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>FAX</td>
								<td><input type="text" name="orderfax" id="orderfax"
									value="<?php if (isset($data['orderfax'])){ echo $data['orderfax'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td>Phone</td>
								<td><input type="text" name="orderphone" id="orderphone"
									value="<?php if (isset($data['orderphone'])){ echo $data['orderphone'];}else {echo "";}?>">
								</td>
							</tr>
							<tr>
								<td><input type="submit" name="submit" id="submit_pharminfo">
								</td>
							</tr>
						</table>

					</form>
				</div>
				
				<input type="hidden" value="<?php echo $_SESSION['userId']; ?>"
					id="userId" />

				<div id="loader-human">					
					<div id="loader-inner" style="height:<?php echo floor($fill);?>% ;top:<?php echo floor(100-$fill);?>%;z-index:0"></div>
					<div id="loader-image" style="z-index:1"></div>
					<!-- <img src="images/human.png" width="120" height="300" /> -->
				</div>
			</div>
			<a href="<?php echo base_url();?>login/logout">Logout</a>