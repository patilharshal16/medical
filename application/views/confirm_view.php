<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" href="<?php echo base_url();?>styles/style.css" />
	<title>Medical Diary</title>
</head>
<body>
	<div class="logo"></div>
	<div class="container">
		
		<div class="message">
			We've sent you a Confirmation Link on your email.<br/>
			Please click on that link to confirm your registration.<br/>
			
		</div>
		
	</div>
</body>
</html>
