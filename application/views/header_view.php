<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/main-frame.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/style.css">
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/profile.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/save.js"></script>
<title>Medical Diary</title>
</head>

<body>
	<div id="diary">
		<!-- Closed in right-menu.php -->
		<div id="diary-inner">