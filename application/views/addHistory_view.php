
<div id="inner">
	<form action="<?php echo base_url();?>history/addHistory" method="POST"
		enctype="multipart/form-data">
		<table width="100%">
			<tr>
				<td>History Name</td>
				<td>:</td>
				<td><input type="text" name="historyName" id="historyName"
					placeholder="Add History Name" /></td>
			</tr>
			<tr>
				<td>Select Department</td>
				<td>:</td>
				<td><select name="department" id="department">
						<?php foreach ($data as $d){?>
						<option value="<?php echo $d->dept_id;?>">
							<?php echo $d->departmentName;?>
						</option>
						<?php }?>
				</select></td>
			</tr>
			<tr>
				<td>Date of Treatment</td>
				<td>:</td>
				<td><input type="date" id="treatmentDate" name="treatmentDate"></td>
			</tr>
			<tr>
				<td>Hospital Name</td>
				<td>:</td>
				<td><input type="text" id="hospitalName" name="hospitalName">
				</td>
			</tr>
			<tr>
				<td>Doctor Name</td>
				<td>:</td>
				<td><input type="text" id="doctorName" name="doctorName">
				</td>
			</tr>
			<tr>
				<td>Upload File</td>
				<td>:</td>
				<td><input type="file" id="historyDocument" name="historyDocument" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><input type="submit" value="add-History" /></td>
			</tr>
		</table>
	</form>
</div>
