$(document).ready(function(){
	$("#inner-about").click(function(e){
		e.preventDefault();
		
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#about-form").show();
	});
	
	
	$("#inner-contact-info").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#contact-form").show();
	});	
	
	$("#inner-insurance-info").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#ins-info-form").show();
	});

	$("#inner-medications").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#medication-form").show();
	});

	$("#inner-allergies").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#allergies-form").show();
	});

	$("#inner-symptooms-cond").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#sympt-cond-form").show();
	});

	$("#inner-proc").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#proc-form").show();
	});

	$("#inner-immune").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#immune-form").show();
	});

	$("#inner-pharma-info").click(function(e){
		e.preventDefault();
		$(".profile-forms").each(function(){
			$(this).hide();
		});
		$(".btn-tab").each(function(){
				$(this).removeClass("active");
		});
		$(this).addClass('active');
		$("#pharm-info-form").show();
	});
	
});
