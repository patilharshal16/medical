$(document).ready(function() {
	var base_url = "http://localhost/medicalDairy/";
	var userId = $("#userId").val();
	$("#submit_about").click(function(e) {
		e.preventDefault();
		var fname = $("#fname").val();
		var lname = $("#lname").val();
		var sex = $("#sex").val();
		var birthday = $("#birthday").val();
		var location = $("#location").val();
		var zip = $("#zip").val();
		var photo = $("#photo").val();
		var height1 = $("#height1").val();
		var height2 = $("#height2").val();
		var weight = $("#weight").val();
		var bloodgroup = $("#bloodgroup").val();
		
		var data = {
			userId : userId,
			fname : fname,
			lname : lname,
			sex : sex,
			birthday : birthday,
			location : location,
			zip : zip,
			photo : photo,
			height1 : height1,
			height2 : height2,
			weight : weight,
			bloodgroup : bloodgroup
		};
		//console.log(data);
		$.ajax({
			url : 'profile/saveabout',
			data : data,
			type : 'POST',
			success : function(msg) {
				alert(msg);
			}
		});
		
		updateProfilePercentage();
	});

	$("#submit_contactinfo").click(function(e) {
		e.preventDefault();
		var address1 = $("#address1").val();
		var address2 = $("#address2").val();
		var city = $("#city").val();
		var state = $("#state").val();
		var zip = $("#contact_zip").val();
		var number = $("#number").val();
		var name = $("#name").val();
		var relation = $("#relation").val();
		var contact = $("#contact").val();
		var emergencyaddress1 = $("#emergencyaddress1").val();
		var emergencyaddress2 = $("#emergencyaddress2").val();
		var emergencycity = $("#emergencycity").val();
		var emergencystate = $("#emergencystate").val();
		var emergencyzip = $("#emergencyzip").val();

		var data = {
			userId : userId,
			address1 : address1,
			address2 : address2,
			city : city,
			state : state,
			zip : zip,
			phoneno : number,
			emergencyname : name,
			emergencyrelation : relation,
			emergencyphone : contact,
			emergencyaddress1 : emergencyaddress1,
			emergencyaddress2 : emergencyaddress2,
			emergencycity : emergencycity,
			emergencystate : emergencystate,
			emergencyzip : emergencyzip
		};
	//	console.log(data);
		$.ajax({
			url : base_url + 'profile/savecontact',
			data : data,
			type : 'POST',
			success : function(msg) {
				alert(msg);
			}
		});

		updateProfilePercentage();
	});

	$("#submit_insuranceinfo").click(function(e) {
		e.preventDefault();
		var provider = $("#provider").val();
		var name = $("#policy-holder-name").val();
		var bdate = $("#bdate").val();
		var policy = $("#policy").val();
		var group = $("#group").val();
		var ename = $("#ename").val();
		var worknumber = $("#worknumber").val();
		var address1 = $("#empaddress1").val();
		var address2 = $("#empaddress2").val();
		var city = $("#empcity").val();
		var state = $("#empstate").val();
		var zip = $("#empzip").val();

		var data = {
			userId : userId,
			insurenceprovider : provider,
			policyholdername : name,
			policyholderbirthdate : bdate,
			policy : policy,
			group : group,
			employername : ename,
			employerworkno : worknumber,
			employeraddress1 : address1,
			employeraddress2 : address2,
			employercity : city,
			employerstate : state,
			employerzip : zip
		};
		// console.log(data);
		$.ajax({
			url : base_url + 'profile/saveinsurance',
			data : data,
			type : 'POST',
			success : function(msg) {
				alert(msg);
			}
		});
		
		updateProfilePercentage();
	});

	$("#submit_medication").click(function(e) {
		e.preventDefault();
		var name = $("#medication_name").val();
		var notes= $("#notes").val();
		var stilltaking=$("#stilltaking").val();
		
		var data = {
			userId : userId,
			medication:	name,
			notes: 	notes,
			stilltaking: stilltaking
		};
		console.log(data);
		$.ajax({
			url: 'profile/savemedication',
			data: data,
			type: 'POST',
			success: function(msg){
				alert(msg);
			}
		});
		
		updateProfilePercentage();
	});

	$("#submit_allergies").click(function(e) {
		e.preventDefault();
		var allergy = $("#allergy").val();
		var notes= $("#allergy_notes").val();
		var nothaving=$("#nothaving").val();
		
		var data = {
			userId : userId,
			allergy: allergy,
			notes: notes,
			currentllhaving: nothaving
		};
		console.log(data);
		
		$.ajax({
			url: 'profile/saveallergy',
			data: data,
			type: 'POST',
			success: function(msg){
				alert(msg);
			}
		});
		
		updateProfilePercentage();
	});

	$("#submit_symptcond").click(function(e) {
		e.preventDefault();
		var symptom = $("#symptom").val();
		var notes = $("#symptom_notes").val();
		var having = $("#NotHaving").val();
		
		var data = {
				userId : userId,
				symptom : symptom,
				notes : notes,
				having : having
		};
		
		$.ajax({
			url : 'profile/savesymptom',
			data: data,
			type: 'POST',
			success: function(msg){
				alert(msg);
			}
		});
		
		updateProfilePercentage();
	});

	$("#submit_proc").click(function(e) {
		e.preventDefault();
		var name = $("#procedure_name").val();
		var notes = $("#procedure_notes").val();
		var date = $("#procedure_date").val();
		var having = $("#NotHaving").val();
		
		var data = {
				userId : userId,
				name : name,
				date : date,
				notes : notes,
				having : having
		};
		
		$.ajax({
			url : 'profile/saveProcedure',
			data: data,
			type: 'POST',
			success: function(msg){
				alert(msg);
			}
		});
		
		updateProfilePercentage();
	});

	$("#submit_immune").click(function(e) {
		e.preventDefault();
		var name= $("#immunization").val();
		var notes= $("#immunizationNotes").val();
		var date= $("#immunizationDate").val();
		
		var data = {
				userId : userId,
				name : name,
				notes : notes,
				date : date
		};
		$.ajax({
			url : 'profile/saveimmunization',
			data: data,
			type: 'POST',
			success: function(msg){
				alert(msg);
			}
		});
		updateProfilePercentage();

	});

	$("#submit_pharminfo").click(function(e) {
		e.preventDefault();
		
		var name = $("#pharmacyname").val();
		var  fax= $("#fax").val();
		var  phone= $("#phone").val();
		var  mail= $("#orderpharmacy").val();
		var  order_fax= $("#orderfax").val();
		var  order_phone= $("#orderphone").val();
		
		var data ={
				userId : userId,
				name: name,
				fax : fax,
				phone : phone,
				mail : mail,
				order_fax : order_fax,
				order_phone : order_phone
		}
		
		$.ajax({
			url : 'profile/savepharmacy',
			data : data,
			type : 'POST',
			success : function(msg){
				alert(msg);
			}
		});
		
		updateProfilePercentage();
	});

	
	function updateProfilePercentage(){
		var data ={
				userId : userId
		};
		$.ajax({
			url: 'profile/getPerc',
			data: data,
			type: 'POST',
			success: function(msg){
				alert(Math.floor(msg));
				$("#loader-inner").css("height",Math.floor(msg)+"%");
				$("#loader-inner").css("top",100-Math.floor(msg)+"%")
			}
		});		
	}
	
	
});
